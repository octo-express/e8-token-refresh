module.exports = ( req, res, next ) ->

  send = res.send

  res.send = ( body ) ->
    res.header 'token', req.token if req.token

    send.call @, body

  next()

